![输入图片说明](https://images.gitee.com/uploads/images/2021/0812/160953_07f25814_8734150.jpeg "稿定设计-2_副本.jpg")

## 你提需求，Gitee 来做

你有什么好玩的表情包创意吗？

中午吃饭的时候、出去嗨的时候，需要马建仓帮你呼朋唤友吗？

和产品经理Battle的时候，需要马建仓为你加油助威吗？

现在，你👉 **只需提供「文案」** 👈，就有可能获得想要的「Gitee马建仓-表情包」哦！

动动手指，参与本仓库「悬赏Issues」、提交PR，赢取888元现金奖励或者Gitee周边大礼包吧！

[参与流程>>>](https://gitee.com/gitee-community/majiancang?_from=gitee_search#参与流程)

[悬赏Issues入口>>>](https://gitee.com/gitee-community/majiancang/issues/I45YXG?from=project-issue)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/105743_b9c9ec76_8734150.gif "删库跑路啦1.gif")

btw，已有的表情包放在最后啦，如需自取。🤪


## 🚀活动概述

1. 活动形式：有奖征集🍗
2. 面向人群：所有 Gitee 平台用户都可参与！（[快速注册通道](http://gitee.com/signup?from=homepage)）
3. 内容要求：（敲黑板啦❗❗）

① 直接在现有的表情包动作上修改文案即可！（[表情包通道](http://gitee.com/gitee-community/majiancang#%E9%A9%AC%E5%BB%BA%E4%BB%93%E8%A1%A8%E6%83%85%E5%8C%85)）

② 或者是新动作+新文案，投稿时请附个简单的示意图。

③  **与 Gitee 或程序员有关** 的好玩、有趣的创意。🤔

## 🎊活动时间

1. 投稿和评选时间：2021年8月18日-2021年8月31日
2. 结果公布时间：2021年9月1日-2021年9月3日（点个⭐Star不迷路）
3. 奖品发出时间：2021年9月6日-2021年9月10日

## 🎁活动奖励

1. 最佳创意奖：888元赏金（1名）
2. 最佳贡献奖：Gitee 周边大礼包（5名），包含限量版新鲜出炉的马建仓大鼠标垫！
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/101910_b55757fb_8734150.png "屏幕截图.png")

## 🎀参与流程

1. 报名方式：[报名直接戳我](https://gitee.com/gitee-community/majiancang/issues/I45YXG?from=project-issue)，进入本次活动的悬赏页面，点击【我要参与】即为报名成功。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/152224_47d556e9_8734150.png "屏幕截图.png")
2. 投稿流程：

① 直接在本仓库下点击文件[【投稿请直接编辑我😁】](http://gitee.com/gitee-community/majiancang/blob/master/%E6%8A%95%E7%A8%BF%E8%AF%B7%E7%9B%B4%E6%8E%A5%E7%BC%96%E8%BE%91%E6%88%91%F0%9F%98%81.md)- 点击【编辑】，进入编辑页面，输入你的表情包名称和创意内容；（❌标题中的.md请勿删除）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/133109_bfc5aecf_1850385.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/132129_c26c95bc_1850385.png "屏幕截图.png")
② 完成以上输入后便可【提交审核】，此时系统会自动生成一个轻量级 PR；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/133032_218ddfb0_1850385.png "屏幕截图.png")
③ 回到之前报名的 [issue 地址](https://gitee.com/gitee-community/majiancang/issues/I45YXG?from=project-issue)，点击【提交代码】，并选中上一步中生成的 PR 地址，即为投稿成功啦~😎
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/154140_993805e4_8734150.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/154213_1dd95b34_8734150.png "屏幕截图.png")
3. 接下来你要做的就是拉票拉票拉票！ **将 PR 地址分享至交流群、自己的好友或者是微信朋友圈** ，以便为你的投稿 PR 获取更多点赞，助你赢得头筹！🎊
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/132524_d981aa31_1850385.png "屏幕截图.png")

## 🎉评选方式

1. 采取 Gitee 社区用户投票的方式评选， **投稿 PR 累计获👍数** 排名靠前的用户即为本次创意征集的获胜者；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/134736_8b5da876_1850385.png "屏幕截图.png")
2. 获得最佳创意奖的用户，其投稿的创意必须包含“新动作+新文案”，最佳贡献奖不做要求；
3. 所有获胜作品必须符合内容要求，否则视为无效。


## 🚨注意事项

1. 所有投稿内容必须为原创，不得抄袭他人，如经发现雷同，以提交时间靠前的为有效；
2. 刷票行为是可耻行为，一经发现取消参与资格；
3. 悬赏金额包含平台服务费等，最终所得赏金以实际到账为准；
4. 本活动最终解释权归 Gitee 平台所有。

## 📢马建仓表情包
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/105729_91b8e1fb_8734150.gif "出 Bug 啦.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/105743_b9c9ec76_8734150.gif "删库跑路啦1.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/105752_5946fa4c_8734150.gif "周末啦.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/105912_8edd7a37_8734150.gif "实现不了1.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/113346_3a7a7c89_8734150.gif "这个问题无法重现.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/113422_24d71e70_8734150.gif "就差一个程序员了1.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/113433_3ea92984_8734150.gif "new 一个对象.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/113444_66f1327b_8734150.gif "PHP 是最好的语言.gif")